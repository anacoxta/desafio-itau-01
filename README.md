## Desafio Itaú 01
# Consolidação de dados usando javascript vanilla

Esse é o desafio 1 de 3, em que será avaliada a **lógica de programação** e **conhecimentos em Javascript**.   
&nbsp;      
      
### **Links de cada um dos desafios:**

* **DESAFIO 01 (este) = HTML + BOOTSTRAP + JAVASCRIPT VANILLA**
  * **Repositório: https://gitlab.com/anacoxta/desafio-itau-01**
  * **Deploy: https://desafio-itau-01.netlify.com/**
* DESAFIO 2 = REACT + BULMA CSS + STYLED-COMPONENTS
  * Repositório: https://gitlab.com/anacoxta/desafio-itau-02
  * Deploy: https://desafio-itau-02.netlify.com
* DESAFIO 3 = REACT + MATERIAL-UI + STYLED-COMPONENTS
  * Repositório: https://gitlab.com/anacoxta/desafio-itau-03 _(work in progress)_
  * Deploy: https://desafio-itau-03.netlify.com/ _(work in progress)_      
&nbsp;   


      
### Cenário Proposto
*Neste projeto temos uma página estática em html, que mostra o histórico de lançamentos cartão de um cliente, de forma desorganizada.
A necessidade do projeto é criar um consolidado de gastos por mês que fique facilmente visível para o usuário nesta página.*   
&nbsp;      
      
### Requisitos

- [x] A lógica de consolidação dos meses deve ser feita em Javascript;
- [x] Essa logica deve ser executada no carregamento da página gerando uma tabela **Mês | Total Gasto**;
- [x] O campo Mês deve ser exibido por escrito;
- [x] Não é permitido usar nenhum framework para realizar essa tarefa;
- [x] Os dados consolidados devem ser apresentados em formato de tabela na mesma página html;
- [x] Só devem ser exibidos na tabela de gastos consolidados os meses em que houveram lançamentos.
&nbsp;      
&nbsp;      
&nbsp;      
      
     
      
# Instruções

Para rodar o código localmente...
1) [Clone](https://help.github.com/pt/github/creating-cloning-and-archiving-repositories/cloning-a-repository) este repositório em sua máquina;
2) Dentro do repositório local, localize e abra o arquivo **index.html** no navegador.   
&nbsp;   
&nbsp;   


### **Observação**:
O requisito mínimo para rodar este projeto é:
* Um navegador de sua preferência
/* ===========================
OBSERVAÇÃO:
CONSIDERANDO OS DADOS FORNECIDOS LEVANTEI A HIPÓTESE DE QUE ELES SE REFEREM AOS LANÇAMENTOS DOS ÚLTIMOS 12 MESES.
Isto significa que estamos lidando com dados deste ano (no caso, 2020) e do ano anterior (2019).
Levando em conta que estamos em abril, todo mês que for maior que "04" (ex: maio, junho, etc) se refere, na realidade,
ao ano anterior. Usei esta hipótese para ordenar os dados na tabela consolidada.
============================== */

const entryTable = document.getElementById("tb_fatura");
const monthlyTable = document.getElementById("tb_mensal");

// Organiza os dados da tabela de lançamentos em um "array de arrays"
let entryData = [...entryTable.rows].map((t) => [...t.children].map((u) => u.innerText));

// Cria um objeto para receber as somas das transações mensais
let monthlyData = {
  1: 0,
  2: 0,
  3: 0,
  4: 0,
  5: 0,
  6: 0,
  7: 0,
  8: 0,
  9: 0,
  10: 0,
  11: 0,
  12: 0,
};


// Função para substituir o número do mês pelo nome
const nameTheMonth = (num) => {
  switch (num) {
    case 1:
      return "Janeiro";
    case 2:
      return "Fevereiro";
    case 3:
      return "Março";
    case 4:
      return "Abril";
    case 5:
      return "Maio";
    case 6:
      return "Junho";
    case 7:
      return "Julho";
    case 8:
      return "Agosto";
    case 9:
      return "Setembro";
    case 10:
      return "Outubro";
    case 11:
      return "Novembro";
    case 12:
      return "Dezembro";
  }
};


/* =================================
Array "entryData" -> Loop...
  1) Ao passar pelo valor de lançamento, irá tratar o valor;
  2) Ao passar pelo mês, irá pegar o valor do lançamento e
     ...jogar no mês correspondente no objeto "monthlyData"
     ...somando com o subtotal mensal
================================= */
for (let row = 0; row < entryData.length; row++) {
  for (let cell = 0; cell < entryData[row].length; cell++) {
    if (row > 0 && cell === 1) {
      // 1)
      entryData[row][cell] = entryData[row][cell].replace("R$ ", "").replace(".", "").replace(",", ".");
      entryData[row][cell] = parseFloat(entryData[row][cell]);
    } else if (row > 0 && cell === 2) {
      // 2)
      let currentMonth = parseFloat(entryData[row][cell]);
      monthlyData[currentMonth] = monthlyData[currentMonth] + entryData[row][cell - 1];
    }
  }
}


// Determina o mês e o ano atuais
let today = new Date();
let thisMonth = today.getMonth() + 1;
let thisYear = today.getFullYear();

// Variável usada p/ ordenar os meses do ano anterior na nova tabela
let y = 1;


/* =================================
Objeto "monthlyData" -> Loop...
  1) Ignora o mês se a somatória de lançamentos for 0
  2) Cria nova linha na tabela "monthlyTable"
     2.1) se o mês em questão for menor ou igual ao mês atual, sinal que é do ano corrente
          ENTÃO a nova linha é inserida no final da tabela
     2.2) se o mês em questão for maior que o mês atual, sinal que se refere ao ano anterior
          ENTÃO, a nova linha é inserida na posição y (que vai sendo incrementada)
  3) Se o mês em questão for igual ao mês atual, acrescenta classe à linha para diferenciar a cor
  4) Passa o mês em questão pelo switch da função nameTheMonth
  5) Para o mês: cria uma célula e um nó de texto, e vincula os dois
  6) Para a soma dos lançamentos: idem (e aproveita e trata o dado, passando-o pelo 'toLocaleString')
================================= */
  for (month in monthlyData) {
  // 1)
  if (monthlyData[month] > 0) {

    let newRow = "";
  
    // 2)
    if (month <= thisMonth) {
      newRow = monthlyTable.insertRow(-1);
    } else {
      newRow = monthlyTable.insertRow(y);
      monthlyTable.rows.item(y).classList.add("table-light");
      y++;
    }
  
    // 3) 
    if (month == thisMonth) monthlyTable.rows.item(monthlyTable.rows.length - 1).classList.add("table-info");
  
    // 4)
    let m = nameTheMonth(parseFloat(month));

    // 5)
    let monthCell = newRow.insertCell(0);
    let monthText = document.createTextNode(m)
    monthCell.appendChild(monthText);
  
    // 6)
    monthlyData[month] = monthlyData[month].toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})
    let sumCell = newRow.insertCell(1);
    let sumText = document.createTextNode(monthlyData[month]);
    sumCell.appendChild(sumText);
  }
}

